#ifndef NAMEDEVENT_H_INCLUDED
#define NAMEDEVENT_H_INCLUDED

#include <windows.h>

#include "WindowsAPIError.h"

class NamedEvent final
{
public:
    NamedEvent(const char* name, BOOL manual_reset = FALSE) :
        event_{NULL}
    {
        event_ = ::OpenEvent(EVENT_ALL_ACCESS, FALSE, name);
        if (!event_) {
            event_ = ::CreateEvent(nullptr, manual_reset, FALSE, name);
            if (event_ == INVALID_HANDLE_VALUE) {
                throw WindowsAPIError();
            }
        }
    }

    ~NamedEvent()
    {
        if (event_) {
            ::CloseHandle(event_);
        }
    }

public:
    void pulse()
    {
        if (!::PulseEvent(event_)) {
            throw WindowsAPIError();
        }
    }

    bool wait(DWORD timeout = INFINITE)
    {
        DWORD const result = ::WaitForSingleObject(event_, timeout);
        switch (result) {
            case WAIT_OBJECT_0: return true;
            case WAIT_TIMEOUT:  return false;
            default:            throw WindowsAPIError();
        }
    }

private:
    HANDLE event_;
};

#endif // NAMEDEVENT_H_INCLUDED
