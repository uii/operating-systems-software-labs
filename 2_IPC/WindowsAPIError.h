#ifndef WINDOWSAPIERROR_H_INCLUDED
#define WINDOWSAPIERROR_H_INCLUDED

#include <stdexcept>
#include <windows.h>

class WindowsAPIError final:
    public std::exception
{
public:
    WindowsAPIError();            // Uses ::GetLastError() result.
    WindowsAPIError(int code);    // Uses the code specified as an error code.
    virtual ~WindowsAPIError();

public:
    const char* what() const noexcept override;
    int         code() const noexcept;

private:
    int code_;
    LPTSTR buffer_;
};

// --------------------------------------------------------------------
// Implementation details go below.

WindowsAPIError::WindowsAPIError(int code) :
    code_{code}, buffer_{nullptr}
{
    ::FormatMessage(
        FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
        nullptr,
        code,
        0,
        reinterpret_cast<LPTSTR>(&buffer_),
        0,
        nullptr
    );
}

WindowsAPIError::WindowsAPIError() :
    WindowsAPIError(::GetLastError())
{
}

WindowsAPIError::~WindowsAPIError()
{
    if (buffer_)
    {
        ::LocalFree(buffer_);
    }
}

const char* WindowsAPIError::what() const noexcept
{
    return buffer_ ? buffer_ : "<no message available>";
}

int WindowsAPIError::code() const noexcept
{
    return code_;
}

#endif // WINDOWSAPIERROR_H_INCLUDED
