#include <iostream>

#define WINVER 0x0502
#include <windows.h>
#include <sddl.h>

// Students to be given the implementation of these functions,
// so that they could setup security while still focusing on IPC.
static PSECURITY_DESCRIPTOR create_security_descriptor();
static SECURITY_ATTRIBUTES create_security_attributes() __attribute__((unused));

static void print_help(bool is_server);

using std::cin;
using std::cerr;
using std::cout;
using std::endl;
using std::getline;
using std::string;

int main()
{
	string name;
	cout << "Enter full mailslot path: ";
	cin >> name;

	bool is_server = false;
    HANDLE mailslot = ::CreateFile(
			name.c_str(),
			GENERIC_READ | GENERIC_WRITE,
			FILE_SHARE_READ,
			NULL,
			OPEN_EXISTING,
			0,
			NULL);
	if (mailslot == INVALID_HANDLE_VALUE) {
		SECURITY_ATTRIBUTES attributes;
		attributes.nLength = sizeof(attributes);
		attributes.lpSecurityDescriptor = create_security_descriptor();
		attributes.bInheritHandle = FALSE;
		if (!attributes.lpSecurityDescriptor) {
			cerr << "[FAIL] ConvertStringSecurityDescriptorToSecurityDescriptor() failed "
					"with GetLastError() = " << ::GetLastError() << ".\n";
			return EXIT_FAILURE;
		}
		mailslot = CreateMailslot(
				name.c_str(), 0, MAILSLOT_WAIT_FOREVER, &attributes);
		if (mailslot == INVALID_HANDLE_VALUE) {
			cerr << "[FAIL] CreateMailslot() failed with GetLastError() = " << ::GetLastError() << ".\n";
			return EXIT_FAILURE;
		}
		::LocalFree(attributes.lpSecurityDescriptor);
		is_server = true;
	}

	print_help(is_server);
	for (;;) {
		cout << "> ";
		string command;
		if (!(cin >> command)) {
			if (is_server) {
				cerr << "Where are you going, Commander?\n";
			}
			break;
		}
		if (command == "quit") {
			if (is_server) {
				cout << "Farewell, Commander!\n";
			}
			break;
		}
		else if (!is_server && command == "write") {
			cout << "Enter message text. Terminate with an empty line.\n";
			string message, line;
			getline(cin, line);
			do {
				getline(cin, line);
				if (!cin) {
					break;
				}
				message += line + '\n';
			}
			while (line.size());
			DWORD bytes_written;
			BOOL const result = ::WriteFile(
					mailslot, message.c_str(), message.size() + 1, &bytes_written, NULL);
			if (result) {
				cout << "[ OK ] Message sent.\n";
			}
			if (!result) {
				cerr << "[FAIL] WriteFile() failed with GetLastError() = " << GetLastError() << ".\n";
				continue;
			}
		}
		else if (is_server && command == "read") {
			DWORD next_message_size;
            BOOL result = ::GetMailslotInfo(
					mailslot, NULL, &next_message_size, NULL, NULL);
			if (result) {
				if (next_message_size == MAILSLOT_NO_MESSAGE) {
					cout << "[ OK ] No messages for you, Commander.\n";
					continue;
				}
			}
			else {
				cerr << "[FAIL] GetMailslotInfo() failed with GetLastError() = " << GetLastError() << ".\n";
				continue;
			}

			string message(next_message_size, '\0');
			DWORD bytes_read;
			result = ::ReadFile(
					mailslot, &message[0], message.size(), &bytes_read, NULL);
			if (result) {
				cout << '\n' << message << '\n';
			}
			else {
				cerr << "[FAIL] ReadFile() failed with GetLastError() = " << GetLastError() << ".\n";
			}
		}
		else if (command == "check") {
			DWORD message_count;
            BOOL result = ::GetMailslotInfo(
					mailslot, NULL, NULL, &message_count, NULL);
			if (result) {
				if (is_server) {
					if (message_count) {
						cout << "[ OK ] Commander, you have " << message_count << " new "
							 << "message" << (message_count > 1 ? "s" : "")
							 << " on your private terminal.\n";
					}
					else {
						cout << "[ OK ] No messages for you, Commander.\n";
					}
				}
				else {
					cout << "[ OK ] Message count: " << message_count << ".\n";
				}
			}
			else {
				cerr << "[FAIL] GetMailslotInfo() failed with GetLastError() = " << GetLastError() << ".\n";
				continue;
			}
		}
		else {
			if (is_server) {
				cerr << "How may I help you, Commander?\n";
			}
			print_help(is_server);
		}
	}
}


static PSECURITY_DESCRIPTOR create_security_descriptor()
{
	const char* sddl =
        "D:"
            "(A;OICI;GRGW;;;AU)"    // Allow read/write to authenticated users
            "(A;OICI;GA;;;BA)";     // Allow full control to administrators
    PSECURITY_DESCRIPTOR security_descriptor = NULL;
    ConvertStringSecurityDescriptorToSecurityDescriptor(
            sddl, SDDL_REVISION_1, &security_descriptor, NULL);
    return security_descriptor;
}


static SECURITY_ATTRIBUTES create_security_attributes()
{
    SECURITY_ATTRIBUTES attributes;
    attributes.nLength = sizeof(attributes);
    attributes.lpSecurityDescriptor = create_security_descriptor();
    attributes.bInheritHandle = FALSE;
    return attributes;
}


void print_help(bool is_server)
{
	cout << "Type";
	if (is_server)
		cout << "\t`read' to read the next one,\n";
	else
		cout << "\t`write' to write a message,\n";
	cout << "\t`check' to check for messages,\n"
			"\t`quit' to terminate.\n";
}
