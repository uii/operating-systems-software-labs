#include <iostream>
#include <string>

#define WINVER 0x0502
#include <windows.h>

#include "../windows_api_error.h"

using std::cerr;
using std::cin;
using std::cout;
using std::endl;
using std::string;

const char*  PIPE_PREFIX = "\\\\.\\pipe\\";
const size_t BUFFER_SIZE = 4096;

int main()
try
{
    string pipe_name;
    cout << "Enter pipe name (without `" << PIPE_PREFIX << "'): ";
    cin  >> pipe_name;
    pipe_name = PIPE_PREFIX + pipe_name;

    HANDLE const pipe = ::CreateFile(
            pipe_name.c_str(),
            GENERIC_READ | GENERIC_WRITE,
            0, //FILE_SHARE_READ | FILE_SHARE_WRITE,
            NULL,
            OPEN_EXISTING,
            0,//FILE_ATTRIBUTE_NORMAL,
            NULL);
    ASSERT_NO_LAST_ERROR("CreateFile");

    string command;
    getline(cin, command);
    for (;;) {
        cout << "> ";
        getline(cin, command);
        if (!cin) {
            break;
        }

        DWORD bytes_read;
        ::WriteFile(pipe, (LPCVOID)command.data(), command.size() + 1, &bytes_read, NULL);

        if (command == "quit") {
            break;
        }

        string response(256, '\0');
        ::ReadFile(pipe, (LPVOID)response.data(), response.size(), &bytes_read, NULL);
        cout.write(response.data(), bytes_read);
        cout << '\n';
    }

    ::CloseHandle(pipe);
    ASSERT_NO_LAST_ERROR("CloseHandle");

    return 0;
}
catch (windows_api_error const& error) {
    cerr << "ERROR: Windows API (" << error.code() << "): " << error.what() << endl;
}

