#define IPC
// #define SYNC

#ifdef SYNC
#   define ENTER(cs) ::EnterCriticalSection(&cs)
#   define LEAVE(cs)  ::LeaveCriticalSection(&cs)
#else
#   define ENTER(cs)
#   define LEAVE(cs)
#endif

#include <cctype>
#include <cstring>
#include <iostream>
#ifdef SYNC
#   include <queue>
#endif

#include <windows.h>

#include "NamedSharedMemory.h"
#include "WindowsAPIError.h"
#ifdef SYNC
#   include "NamedEvent.h"
#endif

#ifdef SYNC
struct Context
{
    NamedEvent* event;
    NamedSharedMemory* shared_memory;
};

HANDLE event_;
CRITICAL_SECTION cs;
char primary_buffer[BUFFER_SIZE];
char secondary_buffer[BUFFER_SIZE];
LONG events_happened = 0;

DWORD WINAPI waitForMessages(LPVOID context_pointer);
#endif

DWORD constexpr BUFFER_SIZE = 256;

void handle_exceptions();

int main()
try
{
    std::cout << "Enter shared memory name: ";
    std::string shared_memory_name;
    std::cin >> shared_memory_name;

    NamedSharedMemory shared_memory(shared_memory_name.c_str(), BUFFER_SIZE);

#ifdef SYNC
    std::cout << "Enter event name: ";
    std::string event_name;
    std::cin >> event_name;

    NamedEvent event(event_name.c_str(), /*manual_reset=*/TRUE);

    Context context { &event, &shared_memory };

    ::InitializeCriticalSection(&cs);
#endif

    char answer;
    do
    {
#ifdef SYNC
        ::EnterCriticalSection(&cs);
            LONG events_to_display = ::InterlockedExchange(&events_happened, 0);
            while (events_to_display--) {
                char* buffer = events_to_display == 1 ? primary_buffer : secondary_buffer;
                std::cout << "Message was received in background:\n\t"
                          << buffer << "\n\n";
            }
            std::cout << "Please choose an action to do:\n"
                         "\tR)ead message from shared memory;\n"
                         "\tW)rite message to shared memory;\n"
                         "\tWait for write (E)vent on shared memory;\n"
                         "\tQ)uit.\n"
                         "Enter a letter: ";
        ::LeaveCriticalSection(&cs);
#else
        std::cout << "Please choose an action to do:\n"
                     "\tR)ead message from shared memory;\n"
                     "\tW)rite message to shared memory;\n"
                     "\tQ)uit.\n"
                     "Enter a letter: ";
#endif
        std::cin >> answer;
        std::cin.ignore();
        if (!std::cin) {
            break;
        }
        answer = std::tolower(answer);
        switch (answer)
        {
        case 'r':
            ENTER(cs);
                std::cout << "The message says:\n";
                std::cout << '\t' << static_cast<LPCTSTR>(shared_memory.getPointer()) << "\n\n";
            LEAVE(cs);
            break;
        case 'w': {
            ENTER(cs);
                std::cout << "Enter message text: ";
                std::string message;
                std::getline(std::cin, message);
                if (!std::cin) {
                    break;
                }
                if (message.size() >= shared_memory.getSize()) {
                    std::cerr << "Message length is limited to " << shared_memory.getSize() << " bytes!\n";
                    break;
                }
            LEAVE(cs);
            std::copy(
                message.c_str(),
                message.c_str() + message.size() + 1,
                static_cast<LPTSTR>(shared_memory.getPointer()));
#ifdef SYNC
            event.pulse();
#endif
            ENTER(cs);
                std::cout << "OK, message is sent.\n\n";
            LEAVE(cs);
            } break;
#ifdef SYNC
        case 'e':
            ::CreateThread(NULL, 0, &waitForMessages, &context, 0, NULL);
            break;
#endif
        case 'q':
            break;
        default:
            ENTER(cs);
                std::cerr << "Please enter R, W, E, or Q (in any case)!\n";
            LEAVE(cs);
        }
    }
    while (answer != 'q');

    return 0;
}
catch (...)
{
    handle_exceptions();
}


void handle_exceptions()
try
{
    throw;
}
catch (const WindowsAPIError& error)
{
    std::cerr << "Windows API error " << error.code() << ": " << error.what() << std::endl;
}
catch (const std::exception& error)
{
    std::cerr << "ERROR: " << error.what() << std::endl;
}
catch (...)
{
    std::cerr << "ERROR: an unknown error occurred!" << std::endl;
}

#ifdef SYNC
DWORD WINAPI waitForMessages(LPVOID context_pointer)
try
{
    auto context = reinterpret_cast<const Context*>(context_pointer);
    context->event->wait();
    ::EnterCriticalSection(&cs);
        char* buffer = ::InterlockedExchangeAdd(&events_happened, 1) ? secondary_buffer : primary_buffer;
        std::strcpy(buffer, static_cast<LPCTSTR>(context->shared_memory->getPointer()));
    ::LeaveCriticalSection(&cs);
    return 0;
}
catch (...)
{
    handle_exceptions();
}
#endif
