#include <stdio.h>
#include <stdlib.h>

#define WINVER 0x0502
#include <windows.h>

const char PLEASE[] = "please";
const char THANKS[] = "thanks";

void die(const char* user_message);

int main()
{
	HANDLE input_pipe_read_end;
	HANDLE input_pipe_write_end;
	HANDLE output_pipe_read_end;
	HANDLE output_pipe_write_end;

	BOOL result;

	SECURITY_ATTRIBUTES attributes;
    attributes.nLength = sizeof(SECURITY_ATTRIBUTES);
    attributes.bInheritHandle = TRUE;
    attributes.lpSecurityDescriptor = NULL;

	result = CreatePipe(&input_pipe_read_end, &input_pipe_write_end, &attributes, 0);
	if (!result) {
		die("CreatePipe() for STDIN and STDERR failed");
	}
	result = SetHandleInformation(
			input_pipe_write_end,
			HANDLE_FLAG_INHERIT,
			0);
	if (!result) {
		die("SetHandleInformation() for STDIN failed");
	}

	result = CreatePipe(&output_pipe_read_end, &output_pipe_write_end, &attributes, 0);
	if (!result) {
		die("CreatePipe() for STDOUT and STDERR failed");
	}
	result = SetHandleInformation(output_pipe_read_end, HANDLE_FLAG_INHERIT, 0);
	if (!result) {
		die("SetHandleInformation() for STDOUT failed");
	}

	STARTUPINFO startup_info;
	ZeroMemory(&startup_info, sizeof(startup_info));
	startup_info.cb = sizeof(startup_info);
	startup_info.hStdInput = input_pipe_read_end;
	startup_info.hStdOutput = output_pipe_write_end;
	startup_info.hStdError = output_pipe_write_end;
	startup_info.dwFlags |= STARTF_USESTDHANDLES;

	PROCESS_INFORMATION pi;

	result = CreateProcess(
			NULL,  			/* use command line to start the shell */
			"cmd.exe",		/* standard shell */
			NULL,			/* default process security attributes */
			NULL,			/* default thread security attributes */
			TRUE,			/* required by STARTF_USESTDHANDLES */
			0,
			NULL,			/* inherit environment */
			NULL,			/* inherit working directory */
			&startup_info,
			&pi);
	if (!result) {
		die("CreateProcess() failed");
	}
	CloseHandle(pi.hProcess);
	CloseHandle(pi.hThread);

    while (1) {
        char buffer[256];
        DWORD bytes_read, bytes_written;
        char *input = NULL;
        int echo_skipped = 0;

        do {
            result = ReadFile(
                    output_pipe_read_end,
                    buffer,
                    sizeof(buffer),
                    &bytes_read,
                    NULL);
            if (!result)
                die("ReadFile() failed");

            if (!echo_skipped) {
                size_t bytes_to_skip = 0;
                while (bytes_to_skip < bytes_read && buffer[bytes_to_skip] != '\n')
                    ++bytes_to_skip;
                if (bytes_to_skip < bytes_read) {
                    echo_skipped = 1;
                    ++bytes_to_skip;
                    fwrite(buffer + bytes_to_skip, bytes_read - bytes_to_skip, 1, stdout);
                }
            } else {
                fwrite(buffer, bytes_read, 1, stdout);
            }
        } while (bytes_read && buffer[bytes_read-1] != '>');

        while (!input) {
            fgets(buffer, sizeof(buffer), stdin);
            if (!strncmp(buffer, THANKS, sizeof(THANKS)-1)) {
                break;
            }
            else if (strncmp(buffer, PLEASE, sizeof(PLEASE)-1)) {
                fprintf(stderr, "Please ask politely!\n> ");
            }
            else {
                input = buffer + sizeof(PLEASE);
            }
        }

        if (!input)
            break;

        result = WriteFile(
                input_pipe_write_end,
                input,
                strlen(input),
                &bytes_written,
                NULL);
        if (!result)
            die("WriteFile() failed");
    }

	CloseHandle(input_pipe_write_end);
	CloseHandle(output_pipe_read_end);

    return 0;
}


void die(const char* user_message)
{
	DWORD error_code = GetLastError();
	if (error_code) {
		LPSTR system_message = "<no system message>";
		DWORD result = FormatMessage(
			FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
			NULL,
			error_code,
			0,
			(LPTSTR)&system_message,
			0,
			NULL);
		if (result) {
            CharToOem(system_message, system_message);
			fprintf(
					stderr,
					"ERROR: %s: (%lu) %s\n",
					user_message, error_code, system_message);
			LocalFree(system_message);
		} else {
			DWORD format_error = GetLastError();
			fprintf(
					stderr,
					"ERROR: %s: (%lu) <failed to obtain error message (%lu)>\n",
					user_message, error_code, format_error);
		}
	} else {
		fprintf(stderr, "ERROR: %s (no OS error detected).\n", user_message);
	}
    ExitProcess(EXIT_FAILURE);
}
