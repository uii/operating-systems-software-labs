#include <iostream>
#include <map>
#include <string>
#include <sstream>

#define WINVER 0x0502
#include <windows.h>

#include "../windows_api_error.h"

using namespace std;

#define CHECK(place) \
    do { \
        int const code = ::GetLastError(); \
        if (code != ERROR_SUCCESS) { \
            cerr << '\n' << place << " @ " << __LINE__ << ": GLE = " << code << endl; \
            std::exit(127); \
        } \
    } while (0)

const char*  PIPE_PREFIX = "\\\\.\\pipe\\";
const size_t BUFFER_SIZE = 4096;

int main()
try
{
    map<string, string> storage;

    string pipe_name;
    cout << "Enter pipe name (without `" << PIPE_PREFIX << "'): ";
    cin  >> pipe_name;
    pipe_name = PIPE_PREFIX + pipe_name;

    HANDLE const pipe = ::CreateNamedPipe(
                            pipe_name.c_str(),
                            PIPE_ACCESS_DUPLEX,
                            PIPE_TYPE_MESSAGE | PIPE_READMODE_MESSAGE | PIPE_WAIT,
                            2,
                            BUFFER_SIZE,
                            BUFFER_SIZE,
                            0,
                            NULL);
    CHECK("CreateNamedPipe");
    for (;;) {

        clog << "Waiting for a client... ";
        ::ConnectNamedPipe(pipe, NULL);
        ASSERT_NO_LAST_ERROR("ConnectNamedPipe");
        clog << "connected.\n";

        for (;;) {
            clog << "Waiting for command... ";
            char buffer[BUFFER_SIZE];
            DWORD bytes_read;
            ::ZeroMemory(buffer, sizeof(buffer));
            ::ReadFile(
                    pipe,
                    (LPVOID)buffer,
                    BUFFER_SIZE,
                    &bytes_read,
                    NULL);
            if (::GetLastError() == ERROR_BROKEN_PIPE) {
                clog << "Client closed the connection." << endl;
                break;
            }
            clog << "received,\n\t";
            clog.write(buffer, bytes_read);
            clog << '\n';

            istringstream request(buffer);
            ostringstream response;

            string command;
            request >> command;
            if (command == "quit") {
                break;
            }
            else if (command == "list") {
                for (auto const& pair : storage) {
                    response << pair.first << " ";
                }
            }
            else if (command == "set") {
                string key, value;
                request >> key >> value;
                storage[key] = value;
                response << "acknowledged";
            }
            else if (command == "get") {
                string key;
                request >> key;
                auto const iterator = storage.find(key);
                if (iterator != storage.end()) {
                    response << "found " << iterator->second;
                } else {
                    response << "missing";
                }
            } else if (command == "delete") {
                string key;
                request >> key;
                auto const iterator = storage.find(key);
                if (iterator != storage.end()) {
                    storage.erase(iterator);
                    response << "deleted";
                } else {
                    response << "missing";
                }
            }

            string const text = response.str();
            clog << text << "\n";
            DWORD bytes_written;
            ::WriteFile(
                        pipe,
                        (LPCVOID)text.data(),
                        text.size(),
                        &bytes_written,
                        NULL);
        }

        ::DisconnectNamedPipe(pipe);
        ASSERT_NO_LAST_ERROR("DisconnectNamedPipe");
        clog << "Client disconnected.\n";

        cout << "Stop the server [yn]? ";
        string dummy;
        std::getline(cin, dummy);
        char const answer = cin.get();
        if (answer == 'y' || answer == 'Y') {
            break;
        }
    }
    ::CloseHandle(pipe);
    ASSERT_NO_LAST_ERROR("CloseHandle");
    return 0;
}
catch (windows_api_error const& error) {
    cerr << "ERROR: Windows API (" << error.code() << "): " << error.what() << endl;
}
