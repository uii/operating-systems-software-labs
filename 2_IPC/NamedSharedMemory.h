#ifndef NamedSharedMemory_h__
#define NamedSharedMemory_h__

#include <stdexcept>

#include <windows.h>

class NamedSharedMemory final
{
public:
    NamedSharedMemory(LPCTSTR name, DWORD size) :
        handle_{NULL}, memory_{nullptr}, size_{size}
    {
        handle_ = ::OpenFileMapping(FILE_MAP_ALL_ACCESS, FALSE, name);
        if (!handle_) {
            handle_ = ::CreateFileMapping(INVALID_HANDLE_VALUE, NULL, PAGE_READWRITE, 0, size, name);
            if (!handle_) {
                throw std::runtime_error("CreateFileMapping");
            }
        }
        memory_ = ::MapViewOfFile(handle_, FILE_MAP_ALL_ACCESS, 0, 0, size_);
        if (!memory_) {
            throw std::runtime_error("MapViewOfFile");
        }
    }

    ~NamedSharedMemory()
    {
        if (memory_) {
            ::UnmapViewOfFile(memory_);
        }
        if (handle_ != INVALID_HANDLE_VALUE) {
            ::CloseHandle(handle_);
        }
    }

public:
    HANDLE getHandle() const  { return handle_; }
    LPVOID getPointer() const { return memory_; }
    DWORD getSize() const     { return size_; }

private:
    HANDLE handle_;
    LPVOID memory_;
    DWORD size_;
};

#endif // NamedSharedMemory_h__
