#include <algorithm>
#include <iomanip>
#include <iostream>
#include <random>
#include <stdexcept>
#include <vector>

#include <windows.h>

#include "key_hit_waiter.h"

class Heap
{
public:
    Heap(HANDLE const& handle)
        : heap_(handle)
    {
    }

    Heap(size_t initialSize, size_t maximumSize)
    {
        heap_ = ::HeapCreate(0, initialSize, maximumSize);
        if (!heap_) {
            throw std::runtime_error("unable to create heap");
        }
    }

    Heap(const Heap&) = delete;
    Heap& operator=(const Heap&) = delete;

    Heap(Heap&&) = default;
    Heap& operator=(Heap&&) = default;

    virtual ~Heap()
    {
        if (heap_) {
            ::HeapDestroy(heap_);
        }
    }

    operator HANDLE() const
    {
        return heap_;
    }

public:
    class Iterator
    {
    public:
        typedef PROCESS_HEAP_ENTRY        value_type;
        typedef value_type*               pointer;
        typedef value_type&               reference;
        typedef std::forward_iterator_tag iterator_category;

    public:
        Iterator() :
            heap_(INVALID_HANDLE_VALUE), finished_(true)
        {
            entry_.lpData = nullptr;
        }

        Iterator(HANDLE heap) :
            heap_(heap), finished_(false)
        {
            entry_.lpData = nullptr;
            operator++();
        }

    public:
        bool operator!=(const Iterator& other)
        {
            if (other.finished_ && finished_) {
                return false;
            }
            return finished_ || other.entry_.lpData != entry_.lpData;
        }

        Iterator& operator++()
        {
            finished_ = TRUE != ::HeapWalk(heap_, &entry_);
            int const errorCode = GetLastError();
            if (finished_ && errorCode != ERROR_NO_MORE_ITEMS) {
                throw std::runtime_error("heap traversal failed");
            }
            return *this;
        }

        PROCESS_HEAP_ENTRY& operator*()
        {
            return entry_;
        }

        const PROCESS_HEAP_ENTRY& operator*() const
        {
            return entry_;
        }

    private:
        HANDLE heap_;
        PROCESS_HEAP_ENTRY entry_;
        bool finished_;
    };

public:
    Iterator begin() const
    {
        return Iterator(heap_);
    }

    Iterator end() const
    {
        return Iterator();
    }

private:
    HANDLE heap_;
};


class HeapGuard final
{
public:
    HeapGuard(Heap const& heap) :
        heap_(heap)
    {
        ::HeapLock(heap_);
    }

    HeapGuard(HeapGuard const&) = delete;
    HeapGuard& operator=(HeapGuard const&) = delete;

    HeapGuard(HeapGuard&&) = default;
    HeapGuard& operator=(HeapGuard&&) = default;

    ~HeapGuard()
    {
        ::HeapUnlock(heap_);
    }

private:
    Heap const& heap_;
};


int main()
{
    key_hit_waiter waiter;

    waiter.query_mode();

    SIZE_T constexpr HEAP_SIZE      = 24 * 1024;
    SIZE_T constexpr MIN_BLOCK_SIZE = 32;
    SIZE_T constexpr MAX_BLOCK_SIZE = 1024;

    std::srand(GetTickCount());

    waiter.pause();
{
    std::cout << "\t1. Creating a heap\n\n";
    Heap heap(0, HEAP_SIZE);

    waiter.pause();

    std::cout << "\t2. Consuming all memory in the heap\n\n";
    while (true)
    {
        SIZE_T const size =
            MIN_BLOCK_SIZE + std::rand() % (MAX_BLOCK_SIZE - MIN_BLOCK_SIZE);
        LPVOID const block = ::HeapAlloc(heap, 0, size);
        if (!block)
            break;
    }

    std::cout << "\t3. Measuring heap memory consumption\n";
    SIZE_T total_allocated = 0;
    SIZE_T total_overhead = 0;
    for (auto& entry : heap)
    {
       if (entry.wFlags & PROCESS_HEAP_ENTRY_BUSY)
       {
           total_allocated += entry.cbData;
           total_overhead  += entry.cbOverhead;
       }
    }
    SIZE_T total_consumed = total_allocated + total_overhead;
    std::cout << "Total allocated:  " << total_allocated            << '\n'
              << "Total overhead:   " << total_overhead             << '\n'
              << "Total consumed:   " << total_consumed             << '\n'
              << "Residual amount:  " << HEAP_SIZE - total_consumed << '\n'
              << '\n';

    std::cout << "\t4. Partially deallocating heap blocks\n\n";
    LPVOID victim = nullptr;
    for (auto& entry : heap)
    {
       if (entry.wFlags & PROCESS_HEAP_ENTRY_BUSY)
       {
           if (victim)
           {
               ::HeapFree(heap, 0, victim);
               victim = nullptr;
           }
           if (std::rand() % 3 == 0)
           {
               victim = entry.lpData;
           }
       }
    }

    waiter.pause();

    std::cout << "\t5. Measuring new heap memory consumption\n";
    total_allocated = 0;
    total_overhead = 0;
    SIZE_T total_free = 0;
    SIZE_T max_free = 0;
    for (auto& entry : heap)
    {
       if (entry.wFlags & PROCESS_HEAP_ENTRY_BUSY)
       {
           total_allocated += entry.cbData;
           total_overhead  += entry.cbOverhead;
       }
       else if (!entry.wFlags)
       {
           total_free += entry.cbData;
           max_free = std::max(max_free, entry.cbData);
       }
    }
    total_consumed = total_allocated + total_overhead;
    std::cout << "Total allocated:    " << total_allocated                  << '\n'
              << "Total overhead:     " << total_overhead                   << '\n'
              << "Total consumed:     " << total_consumed                   << '\n'
              << "Residual amount:    " << HEAP_SIZE - total_consumed       << '\n'
              << "Total free:         " << total_free                       << '\n'
              << "Largest free block: " << max_free                         << '\n'
              << '\n';


    std::cout << "\t6. Investigating heap fragmentation issues\n";
    SIZE_T const gap = 16;
    std::cout << "Allocating " << max_free - gap << " bytes... ";
    if (LPVOID block = ::HeapAlloc(heap, 0, max_free - gap))
    {
        std::cout << "OK.\n";
        ::HeapFree(heap, 0, block);

        std::cout << "Allocating " << total_free - gap << " bytes... ";
        block = ::HeapAlloc(heap, 0, total_free - gap);
        if (!block)
        {
            std::cout << "FAILED.\n";
        }
        else
        {
            std::cout << "OK. LOLWUT?\n";
        }
    }
    else
    {
        std::cout << "FAILED!\n";
    }

    waiter.pause();

    std::cout << "\n\t7. Destroying the heap\n\n";
}
    waiter.pause();

    std::cout << "\t8. Investigating allocation overhead\n";

    Heap test_heap(0, HEAP_SIZE);

    SIZE_T size = 1;
    while (true)
    {
        LPVOID const block = ::HeapAlloc(test_heap, 0, size++);
        if (!block)
            break;
    }

    std::cout << "\nBlock size\tOverhead\n";
    for (auto& entry : test_heap)
    {
       if (entry.wFlags & PROCESS_HEAP_ENTRY_BUSY)
       {
           std::cout << entry.cbData                          << "\t\t"
                     << static_cast<SIZE_T>(entry.cbOverhead) << '\n';
       }
    }

    waiter.pause(true);
}

