#include <iomanip>
#include <iostream>

#include <windows.h>

#include "key_hit_waiter.h"

void put_protection(std::ostream& output, DWORD const& protection);

int main()
{
    std::cout << "NOTE: this is variant 8 of task 3,\n"
              << "because it requires both GetSystemInfo() and VirtualQuery().\n\n"
              << "Legend:\n"
              << "\tState:  Free, Reserved, Committed;\n"
              << "\tAccess: Readable, Writable, eXecutable, Copy-on-write, Guard,\n"
              << "\t        No cache, write combine (Q), no access (-------);\n"
              << "\t        empty space indicates no access to page protection info.\n\n";

    SYSTEM_INFO system_info;
    ::GetSystemInfo(&system_info);

    std::cout << std::setw(10) << std::right << "Address"
              << std::setw(12) << std::right << "Size"
              << " Used"
              << " State"
              << std::setw(8) << std::left << " Access"
              << " Type"
              << '\n';

    auto current_region = reinterpret_cast<const BYTE*>(system_info.lpMinimumApplicationAddress);
    while (current_region < system_info.lpMaximumApplicationAddress)
    {
        MEMORY_BASIC_INFORMATION region_info;
        SIZE_T const info_size = ::VirtualQuery(
            current_region, &region_info, sizeof region_info);
        if (!info_size)
        {
            std::cerr << "Failed to obtain information about region at "
                      << std::hex << reinterpret_cast<const void*>(current_region) << std::dec << "!\n";
            break;
        }
        else
        {
            std::cout << std::hex << std::setw(10) << std::right << region_info.BaseAddress
                      << std::dec << std::setw(12) << std::right << region_info.RegionSize
                      << ' ' << (region_info.AllocationBase ? "yes" : "no ") << ' '
                      << "   "
                        << (region_info.State & MEM_FREE    ? "F" :
                            region_info.State & MEM_RESERVE ? "R" :
                            region_info.State & MEM_COMMIT  ? "C" :
                                                              "")
                      << "   ";
            put_protection(std::cout, region_info.Protect);
            std::cout << ' '
                        << (region_info.Type == MEM_IMAGE ?   "image" :
                            region_info.Type == MEM_MAPPED ?  "mapped" :
                            region_info.Type == MEM_PRIVATE ? "private" :
                                                              "unused")
                      << '\n';

            current_region += region_info.RegionSize;
        }
    }

    key_hit_waiter waiter;
    waiter.pause(true);
}

void put_protection(std::ostream& output, DWORD const& protection)
{
    if (!protection)
    {
        output << "       ";
        return;
    }

    if (protection & ~PAGE_NOACCESS &&
        protection & ~PAGE_EXECUTE)
    {
        output << 'R';
    }
    else
    {
        output << '-';
    }

    if (protection & PAGE_EXECUTE_READWRITE ||
        protection & PAGE_READWRITE)
    {
        output << 'W';
    }
    else
    {
        output << '-';
    }

    if (protection & PAGE_EXECUTE           ||
        protection & PAGE_EXECUTE_READ      ||
        protection & PAGE_EXECUTE_READWRITE ||
        protection & PAGE_EXECUTE_WRITECOPY)
    {
        output << 'X';
    }
    else
    {
        output << '-';
    }

    if (protection & PAGE_WRITECOPY ||
        protection & PAGE_EXECUTE_WRITECOPY)
    {
        output << 'C';
    }
    else
    {
        output << '-';
    }

    output << (protection & PAGE_GUARD        ? 'G' : '-')
           << (protection & PAGE_NOCACHE      ? 'N' : '-')
           << (protection & PAGE_WRITECOMBINE ? 'Q' : '-');
}
