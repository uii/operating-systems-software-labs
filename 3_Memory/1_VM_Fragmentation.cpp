#include <algorithm>
#include <iostream>
#include <vector>

#include <windows.h>

#include "key_hit_waiter.h"

constexpr unsigned long long operator"" _KB(unsigned long long value) { return        1024 * value; }
constexpr unsigned long long operator"" _MB(unsigned long long value) { return 1024 * 1024 * value; }

ULONGLONG estimate_memory_reserves()
{
    MEMORYSTATUSEX status;
    status.dwLength = sizeof status;
    ::GlobalMemoryStatusEx(&status);
    std::cout << "Total VM bytes:     " << status.ullTotalVirtual << '\n'
              << "Available VM bytes: " << status.ullAvailVirtual << '\n';
    return status.ullAvailVirtual;
}

int main()
{
    key_hit_waiter waiter;
    waiter.query_mode();

    LPVOID huge_region = nullptr;

    enum { Fragmentation, Reservation, Finish } step = Fragmentation;
    while (step != Finish)
    {
        std::cout << "\t1. In the beginning:\n";
        ULONGLONG initial_vm = estimate_memory_reserves();
        std::cout << '\n';

        waiter.pause();

        std::vector<LPVOID> regions;

        while (true)
        {
            LPVOID const region = ::VirtualAlloc(
                nullptr,
                100_MB,
                MEM_RESERVE | MEM_COMMIT,
                PAGE_READWRITE);
            if (region)
                regions.push_back(region);
            else
                break;
        }

        std::cout << "\t3. After full VM utilization:\n";
        ULONGLONG const residual_vm = estimate_memory_reserves();
        std::cout << "Residual VM amount is " << (residual_vm > 100_MB ? ">" : "<") << " 100 MB.\n";
        std::cout << '\n';

        waiter.pause();

        std::size_t i = 0;
        std::remove_if(regions.begin(), regions.end(), [&i](auto&& region)
        {
            if (i++ % 2)
            {
                ::VirtualFree(region, 0, MEM_RELEASE);
                return true;
            }
            return false;
        });

        std::cout << "\t5. After partial VM release:\n";
        ULONGLONG const available_vm = estimate_memory_reserves();
        std::cout << "Available VM amount is " << (available_vm > 100_MB ? ">" : "<") << " 100 MB.\n";
        std::cout << '\n';

        waiter.pause();

        DWORD const amount_to_allocate = available_vm / 4;
        std::cout << "\t6. Attempting to allocate " << amount_to_allocate << " bytes... ";
        huge_region = ::VirtualAlloc(
            huge_region,
            amount_to_allocate,
            huge_region ? MEM_COMMIT : MEM_RESERVE | MEM_COMMIT,
            PAGE_READWRITE);
        if (huge_region)
        {
            std::cout << "SUCCEEDED!\n";
            regions.push_back(huge_region);
        }
        else
        {
            std::cout << "FAILED!\n";
        }
        std::cout << '\n';

        for (auto&& region : regions)
        {
            ::VirtualFree(region, 0, MEM_RELEASE);
        }

        std::cout << "\t8. After complete release:\n";
        ULONGLONG const final_vm = estimate_memory_reserves();
        std::cout << "Final VM amout " << (final_vm < initial_vm ? "<" : ">=") << " initial.\n";
        std::cout << '\n';

        waiter.pause();

        if (step == Fragmentation)
        {
            step = Reservation;
            std::cout << "\t9. Reserving " << amount_to_allocate << " bytes... ";
            huge_region = ::VirtualAlloc(
                nullptr,
                amount_to_allocate,
                MEM_RESERVE,
                PAGE_READWRITE);
            if (huge_region)
            {
                std::cout << "SUCCEEDED!\n";
            }
            else
            {
                std::cout << "FAILED!\n";
            }
            std::cout << '\n';
        }
        else if (step == Reservation)
        {
            step = Finish;
        }
    }

    waiter.pause(true);
}
