#ifndef KEY_HIT_WAITER_H_INCLUDED
#define KEY_HIT_WAITER_H_INCLUDED

#include <iostream>

#include <conio.h>

class key_hit_waiter
{
public:
    key_hit_waiter() :
        wait_(false)
    { }

    void query_mode()
    {
        std::cout << "Would you like the program to pause at some points,\n"
                  << "allowing you to examine results and/or VMMap? [y/n]\n";
        do
        {
            char const answer = getch();
            switch (answer)
            {
            case 'y':
            case 'Y':
                wait_ = true;
                return;
            case 'n':
            case 'N':
                wait_ = false;
                return;
            case -1:
                std::exit(0);
                break;
            default:
                std::cout << "Please press Y or N key!\n";
            }
        }
        while (true);
    }

    void pause(bool force = false)
    {
        if (!wait_ && !force)
            return;
        std::cout << "Press any key to continue...\n\n";
        getch();
    }

private:
    bool wait_;
};

#endif // KEY_HIT_WAITER_H_INCLUDED
